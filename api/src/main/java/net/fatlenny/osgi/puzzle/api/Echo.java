package net.fatlenny.osgi.puzzle.api;

public interface Echo {
    String echo();
}