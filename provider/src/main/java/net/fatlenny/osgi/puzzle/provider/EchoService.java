package net.fatlenny.osgi.puzzle.provider;

import net.fatlenny.osgi.puzzle.api.Echo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EchoService implements Echo {
    private static Logger LOG = LoggerFactory.getLogger(EchoService.class);

    @Override
    public String echo() {
        return String.valueOf(System.currentTimeMillis());
    }

    public void start() {
        LOG.info(EchoService.class.toString() + " started");
    }

}
