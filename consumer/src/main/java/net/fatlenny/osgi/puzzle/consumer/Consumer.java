package net.fatlenny.osgi.puzzle.consumer;

import java.util.TimerTask;

import net.fatlenny.osgi.puzzle.api.Echo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer extends TimerTask {
    private static Logger LOG = LoggerFactory.getLogger(Consumer.class);

    private Echo echoService;

    public Consumer(Echo echoService) {
        this.echoService = echoService;
    }

    @Override
    public void run() {
        LOG.info("Current time in ms: " + echoService.echo());
    }
}
