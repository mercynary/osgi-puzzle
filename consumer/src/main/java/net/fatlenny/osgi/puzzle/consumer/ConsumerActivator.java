package net.fatlenny.osgi.puzzle.consumer;

import java.util.Timer;

import net.fatlenny.osgi.puzzle.api.Echo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerActivator {
    private static Logger LOG = LoggerFactory.getLogger(ConsumerActivator.class);

    private Timer timer;
    private Echo echo;

    public ConsumerActivator() {
        this.timer = new Timer();
    }

    public void start() {
        LOG.info(ConsumerActivator.class.toString() + " starting.");
        timer.scheduleAtFixedRate(new Consumer(echo), 5000, 5000);
        LOG.info(ConsumerActivator.class.toString() + " started.");
    }

    public void stop() {
        LOG.info(ConsumerActivator.class.toString() + " stopping.");
        timer.cancel();
        LOG.info(ConsumerActivator.class.toString() + " stopped.");
    }

    public void setEcho(Echo echo) {
        this.echo = echo;
    }
}
